<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->truncate();

         $title = [
             'Apartment',
             'Condominium',
             'Flat',
             'Bungalow'
         ];

         for ($i = 1; $i < 5 ; $i++){
             $now = Carbon\Carbon::now();
             DB::table('categories')->insert(
                 [
                     'title' => $title[array_rand($title)],
                     'created_at'    => $now,
                     'updated_at'    => $now

                 ]
             );
         }
    }
}
