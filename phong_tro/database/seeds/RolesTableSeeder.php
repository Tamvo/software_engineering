<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->truncate();

         $role_name = [
             'user',
             'admin'
         ];

         for ($i = 1; $i < 3 ; $i++){
             $now = Carbon\Carbon::now();
             DB::table('roles')->insert(
                 [
                     'role_name'          => $role_name[array_rand($role_name)],
                     'created_at'    => $now,
                     'updated_at'    => $now

                 ]
             );
         }
    }
}
